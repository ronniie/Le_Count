# Le Count, counting game to the max potential.

**How to install?**


> Windows
> - Step #1 | Install Python (3.6.5 Recommended)
> - Step #2 | Install VirtualENV (If not installed)
> - Step #3 | Download the files to a Folder on your Desktop
> - Step #4 | Go to Command Prompt, and go to the folder on your desktop
> - Step #5 | In CMD Prompt. Type `virtualenv env`
> - Step #6 | Then type `.\env\Scripts\activate`
> - Step #7 | Then type `pip install discord.py`
> - Step #8 | Then run `python main.js `

> MacOS
> - Step #1 | Install Python (3.6.5 Recommended)
> - Step #2 | Install VirtualENV (If not installed; https://sourabhbajaj.com/mac-setup/Python/virtualenv.html)
> - Step #3 | Download the files to a Folder on your Desktop
> - Step #4 | Go to Command Prompt, and go to the folder on your desktop
> - Step #5 | In CMD Prompt. Type `virtualenv env`
> - Step #6 | Then type `source env/bin/activate`
> - Step #7 | Then type `pip install discord.py`
> - Step #8 | Then run `python main.js `

**GitLab -** [https://gitlab.com/BrookeM/Le_Count](https://gitlab.com/BrookeM/Le_Count)

Need Help? Join the discord! [https://chaottiic.com/discord](https://chaottiic.com/discord)